//Static.js => static components like hero section,whycoffee,footer
import { useState } from "react";
import { Cart } from "./App.js";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "./index.css";
import "./query.css";

export function Navbar({ CartProduct }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div className="nav-bar-color">
      {/* ===Nav Start===== */}
      <div className="container">
        <nav className="navbar navbar-expand-lg">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">
              <h1 className="Logo logo-mobile">Leo Coffee</h1>
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav ms-auto">
                <li className="nav-item">
                  <a
                    className="nav-link active text-light"
                    aria-current="page"
                    href="#home"
                  >
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-light" href="#coffee">
                    Coffee
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-light" href="#special">
                    Special
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-light" href="#about">
                    About
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-light" href="#cart">
                    <img
                      src="/Asset/Icons/shopping-bag-02.svg"
                      alt="Shopping cart"
                      onClick={() =>
                        CartProduct.length > 0
                          ? handleShow()
                          : alert("Cart is empty please add any product")
                      }
                    />
                  </a>
                  <Modal show={show} onHide={handleClose}>
                    {/* This whole idea behind is react bootstrap i just modifed little*/}
                    <Modal.Header closeButton>
                      <Modal.Title>Cart</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Cart Finalcartadd={CartProduct} key={CartProduct} />
                    </Modal.Body>
                    <Modal.Footer>
                      <Button
                        className="btn btn-danger"
                        variant="secondary"
                        onClick={() => {
                          alert(
                            "Delete is not Implemented So now it refreshed"
                          );
                          window.location.reload();
                        }}
                      >
                        Delete
                      </Button>
                      <Button variant="primary" onClick={handleClose}>
                        Save Changes
                      </Button>
                    </Modal.Footer>
                  </Modal>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        {/* ===Nav End===== */}
      </div>
    </div>
  );
}

export function Herosection() {
  return (
    <div>
      {/* Hero Section start  */}
      <div className="bg-img">
        <div className="container">
          <div className="container-down">
            <h2 className="main-heading main-heading-mobile">
              Begin your day with <br /> a sip that sparks a <br />
              smile!
            </h2>
            <a href="#">
              <button
                className="buy-now-btn btn btn-warning w3-animate-left"
                style={{
                  borderRadius: "20px",
                  backgroundColor: "#FFD600",
                  width: "140px",
                  height: "45px",
                }}
              >
                Order Now
              </button>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export function WhyOurcoffee() {
  return (
    <div className="container py-5">
      <div className="row py-5">
        <div className="col-8 py-5">
          <h1 className="section-heading">Why Our Coffee</h1>
          <p className="section-content">
            Welcome to Leo Coffee, where passion meets brew. Immerse yourself in
            the <br />
            rich aroma of handcrafted blends, each cup telling a story of
            quality and <br />
            flavor. Explore a diverse menu that caters to every coffee
            connoisseur's <br />
            taste. At Leo, we invite you to experience more than a coffee; we
            offer a <br /> moment of joy in every sip.
          </p>
        </div>
        <div className="col-4 px-5">
          <img
            className="px-5 why-coffee-img"
            src="/Asset/Images/coffee-time.png"
            alt="Why your choosing this Coffee"
          />
        </div>
      </div>
    </div>
  );
}

export function Footer() {
  return (
    <div className="container py-5" id="about">
      <div className="row">
        <div className="col-md-10">
          <h1 className="Logo">Leo Coffee</h1>
          <p className="box-content-dis">
            Embrace the spirit of Leo Coffee, where excellence in coffee
            <br />
            craftsmanship meets a commitment to delight. Our journey <br />
            began with a simple love for the bean, evolving into a haven <br />
            for those seeking an exceptional brew. At Leo, we blend <br />
            tradition with innovation, crafting a coffee experience that <br />
            transcends the ordinary. Join us in celebrating the art and <br />
            passion behind every cup at Leo Coffee.
          </p>
        </div>
        <div className="col-md-2">
          <img
            className="why-coffee-img"
            src="/Asset/Icons/leo-footer.png"
            alt="Leo"
          />
        </div>
      </div>
    </div>
  );
}
