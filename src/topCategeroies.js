import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import "./index.css";
export function HeadingCatergory() {
  return (
    <div className="container py-5 my-2">
      <img
        className="mx-auto d-block py-2"
        src="/Asset/Icons/coffe-icon.svg"
        alt="coffee image"
      />
      <h1 className="section-heading text-center" id="coffee">
        Top Category
      </h1>
    </div>
  );
}
export function Topcategory({ Product }) {
  return (
    <div className="py-5">
      <div className="container">
        <div className="container-box py-5 my-4">
          <div className="box">
            <img
              className="px-5 mx-1"
              style={{ position: "relative", bottom: "60px" }}
              src={Product.image}
              alt="Latte"
            />
            <div
              className="box-content"
              style={{ position: "relative", bottom: "50px" }}
            >
              <p className="box-content-heading text-center">
                {Product.ProductName}
              </p>
              <p className="box-content-dis px-4">{Product.Discription}</p>
              <div className="price-btn py-3">
                <span className="price px-4">{Product.Price} ₹</span>
                <a href="#">
                  <button
                    className="buy-now-btn btn btn-warning"
                    style={{ borderRadius: "20px", backgroundColor: "#FFD600" }}
                  >
                    Buy Now
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
