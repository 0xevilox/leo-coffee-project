import { React, useState, useEffect } from "react";
import ReactDOM from "react-dom/client";
import { Herosection, Navbar, WhyOurcoffee, Footer } from "./Static.js";
import { fetchData } from "./Datafetch.js";
import { HeadingCatergory, Topcategory } from "./topCategeroies.js";
import { Headingproduct, Productcard, WeekendCoffee } from "./App.js";
import "./query.css";

const TopcategoryProducts = [
  {
    id: 1,
    ProductName: "Americano Coffee",
    Discription:
      "Savor the robust simplicity of our Americano harmonious blend of rich espresso and pure hot water, creating a bold and invigorating coffee experience.",
    image: "/Asset/Images/Product/ammerican.png",
    Price: 200,
  },
  {
    id: 2,
    ProductName: "Cappuccino Coffee",
    Discription:
      "Indulge in the creamy perfection of our Cappuccino a luxurious symphony of espresso, velvety steamed milk, and a luscious layer of froth.",
    image: "/Asset/Images/Product/ammerican.png",
    Price: 400,
  },
  {
    id: 3,
    ProductName: "Latte Coffee",
    Discription:
      "Savor the smooth and comforting embrace of our Latte—a harmonious blend of rich espresso and steamed milk, crowned with a delicate froth.",
    image: "/Asset/Images/Product/latte-pd.png",
    Price: 350,
  },
];

const ResueBroder = <div className="borderBottom"></div>;

function MainApp() {
  const [InitalData, UpdateData] = useState([]);
  const [retriveSection, UpdateSection] = useState([]);
  const [AnothertriveSection, AnotherUpdateSection] = useState([]);
  const [CurrentProduct, UpdateProduct] = useState([]);
  useEffect(() => {
    fetchData()
      .then((res) => {
        UpdateData(res);
      })
      .catch((err) => console.log(err));
  }, []);
  useEffect(() => {
    if (true) {
      UpdateSection(InitalData.slice(1, 4));
      AnotherUpdateSection(InitalData.slice(5, 8));
    }
  }, [InitalData]);

  //This technique is called lifting of the state => becuase react one way communication
  function handlerAddtoCart(Productdata, trimmedDescription) {
    const data = [...CurrentProduct];
    const CartData = function (Productdata, trimmedDescription) {
      //Creating the object programmcitally
      const Randomseal = Math.round(Math.random() * 7);
      this.Id = Randomseal;
      this.Name = Productdata.name;
      this.img = Productdata.image_url;
      this.Discription = trimmedDescription;
    };
    const FinalCartData = new CartData(Productdata, trimmedDescription);
    data.push(FinalCartData);
    UpdateProduct(data);
  }
  return (
    <div>
      <Navbar CartProduct={CurrentProduct} />
      <Herosection />
      {ResueBroder}
      <HeadingCatergory />
      <div className="my-container catgerioes-mobile">
        {TopcategoryProducts.map((data) => (
          <Topcategory Product={data} key={data.id} />
        ))}
      </div>
      {ResueBroder}
      <Headingproduct nameHeading="Special Coffee" Id="special" />
      <div className="my-container special-mobile">
        {retriveSection.map((data) => {
          return (
            <Productcard
              key={data.id}
              Productdata={data}
              handlerFunction={handlerAddtoCart}
            />
          );
        })}
      </div>
      {ResueBroder}
      <WhyOurcoffee />
      {ResueBroder}
      <Headingproduct
        nameHeading="Weekend Special Coffee"
        Id="weekendspecial"
      />
      <div className="my-container special-mobile">
        {AnothertriveSection.map((data) => {
          return (
            <WeekendCoffee
              WeekendPro={data}
              key={data.id}
              handlerFunction={handlerAddtoCart}
            />
          );
        })}
      </div>
      {ResueBroder}
      <Footer />
      <footer className="text-center text-white py-3">
        © 2024 Website Made By Vignesh R
      </footer>
    </div>
  );
}

const root = ReactDOM.createRoot(document.querySelector("#root"));
root.render(<MainApp />);
