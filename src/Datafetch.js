export async function fetchData() {
  try {
    const requestData = await fetch("https://fake-coffee-api.vercel.app/api");
    if (requestData.ok) {
      const data = await requestData.json();
      return data;
    } else {
      console.log("Sorry We cannot able to retrive the data check the url");
    }
  } catch (err) {
    console.error("Sorry unable to fetch the data check the internet");
  }
}
