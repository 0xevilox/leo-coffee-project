import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import "./index.css";
import "./query.css";

const Linecart = <div className="bordercartBottom"></div>;

//Heading of the product
export function Headingproduct({ nameHeading, Id }) {
  return (
    <div className="container" id={Id} style={{ paddingTop: "100px" }}>
      <div className="row"></div>
      <h1 className="section-heading text-center">{nameHeading}</h1>
    </div>
  );
}

//Async update product card
export function Productcard({ Productdata, handlerFunction }) {
  const maxLength = 80;
  const trimmedDescription =
    Productdata.description.length > maxLength
      ? Productdata.description.slice(0, maxLength) + "..." //Triming the discription
      : Productdata.description;
  return (
    <div className="container py-5 my-4">
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card" style={{ width: "19rem", height: "27rem" }}>
            <img
              src={Productdata.image_url}
              className="card-img-top"
              alt={Productdata.name}
            />
            <div className="card-body">
              <h5 className="card-title text-center">{Productdata.name}</h5>
              <p className="card-text px-2 my-3">{trimmedDescription}</p>
              <div className="price-btn-container my-5 py-3">
                <span className="price">{Productdata.price + 200} ₹</span>
                <a
                  href="#"
                  className="btn btn-primary btn-warning"
                  id="final-btn"
                  onClick={() =>
                    handlerFunction(Productdata, trimmedDescription)
                  }
                  style={{
                    borderRadius: "20px",
                    backgroundColor: "#FFD600",
                    width: "120px",
                  }}
                >
                  ADD TO CART
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export function WeekendCoffee({ WeekendPro, handlerFunction }) {
  return (
    <Productcard Productdata={WeekendPro} handlerFunction={handlerFunction} />
  );
}
//Add to Cart function
export function Cart({ Finalcartadd }) {
  return (
    <div className="container">
      <div className="row">
        <div className="col-4">
          <div className="test-container">
            {Finalcartadd.map((data) => (
              <img
                src={data?.img}
                alt={data?.Name}
                key={data?.Name}
                style={{ width: "150px" }}
              />
            ))}
          </div>
        </div>
        <div className="col-6 mx-2 py-3 test-container">
          {Finalcartadd.map((data) => (
            <React.Fragment key={data?.Name}>
              <h6 className="card-title-cart">{data?.Name}</h6>
              <p className="card-text-cart">{data?.Discription}</p>
              {/* <button
                type="button"
                className="btn btn-danger btn-sm"
                onClick={() => UpdateDelete(data?.Name)}
              >
                Delete
              </button> */}
              <div className="py-2">{Linecart}</div>
            </React.Fragment>
          ))}
        </div>
      </div>
    </div>
  );
}
